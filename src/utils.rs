use crate::database::connect_to_database;
use crate::{dataset, database, diff, report};
use std::fs::File;
use std::io::Write;

pub async fn test_case(dataset: &str, query: &str) {
    let client = connect_to_database().await;
    let result = client.query(query, &[]).await.expect("database query failed");

    let expected = dataset::load_dataset(dataset);
    let actual = database::rows_to_dataset(result);

    let diff = diff::compare_datasets(expected.clone(), actual.clone());
    let (success, report) = report::generate_report(&diff, &expected, &actual);
    println!("{}", report);

    File::create(format!("reports/{}", dataset)).unwrap().write_all(report.as_bytes()).expect("Failed to write a report");

    if !success {
        panic!("Test case failed, see report for details.");
    }
}