use crate::diff::{DatasetDiff, ColumnDiff};
use crate::dataset::Dataset;

pub fn generate_report(diff: &DatasetDiff, expected_ds: &Dataset, actual_ds: &Dataset) -> (bool, String) {
    if diff.missing_columns.is_empty() && diff.extra_columns.is_empty() && diff.rows_diff.is_empty() && diff.missing_rows.is_empty() && diff.extra_rows.is_empty() {
        return (true, format!("Actual matches expected\nTotal rows: {}", diff.matched_rows));
    }

    let mut report: String = "".to_string();

    if !diff.missing_columns.is_empty() {
        report = format!("{}The following columns are not present, but should be: {}\n", report, diff.missing_columns.join(";"));
    }

    if !diff.extra_columns.is_empty() {
        report = format!("{}The following columns are present, but should not be: {}\n", report, diff.extra_columns.join(";"));
    }

    if !diff.missing_rows.is_empty() {
        report = format!("{}The following rows are missing, but should be present:", report);
        for row in &diff.missing_rows {
            report = format!("{}\n{}", report, row.as_csv(&expected_ds.columns));
        }
        report = format!("{}\n\n", report);
    }

    if !diff.extra_rows.is_empty() {
        report = format!("{}The following rows are present, but shouldn't be:", report);
        for row in &diff.missing_rows {
            report = format!("{}\n{}", report, row.as_csv(&actual_ds.columns));
        }
        report = format!("{}\n\n", report);
    }

    report = format!("{}Total rows matched: {}\n", report, diff.matched_rows);
    report = format!("{}Total rows unmatched: {}\n", report, diff.rows_diff.len());

    if !diff.rows_diff.is_empty() {
        report = format!("{}The following rows did not match:", report);
        for row in &diff.rows_diff {
            report = format!(
                "{}\n---\nColumns: {}\nExpected: {}\nActual: {}\n{}",
                report,
                diff.columns.iter().map(|v| v.name.clone()).collect::<Vec<String>>().join(";"),
                row.expected.as_csv(&expected_ds.columns),
                row.actual.as_csv(&actual_ds.columns),
                report_for_column_diff(&row.column_diff)
            );
        }
    }

    (false, report)
}

fn report_for_column_diff(diff: &Vec<ColumnDiff>) -> String {
    let mut report = "".to_string();

    for column in diff {
        report = format!("{}\t- {}: {} (expected) != {} (actual)\n", report, column.name, column.expected, column.actual);
    }

    report
}