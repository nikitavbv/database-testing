// databsase is prepopulated with following datasets:
// - https://www.kaggle.com/fornaxai/dataadventures
// - https://www.kaggle.com/imkrkannan/mars-weather-data

mod database;
mod dataset;
mod diff;
mod report;
mod utils;

use crate::utils::test_case;

fn main() {
    panic!("this crate is expected to be run with `cargo test`");
}

#[tokio::test]
async fn simple_test() {
    test_case(
        "simple",
        "select distinct atmo_opacity, 1 as \"test_number\" from \"mars-weather\" order by atmo_opacity desc"
    ).await;
}

#[tokio::test]
async fn simple_test_fail() {
    test_case(
        "simple_fail",
        "select cast(max(sunmars_km) as real) as distance, cast(min(ut_ms) as real) as ut_ms from ltdata where ut_ms > 1429920000000"
    ).await;
}

#[tokio::test]
async fn test_mars_weather() {
    test_case(
        "mars_weather",
        "select month, cast(avg(min_temp) as real) as min_temp from \"mars-weather\" where min_temp != double precision 'NaN' group by month  order by month asc"
    ).await;
}

#[tokio::test]
async fn test_ltdata() {
    test_case(
        "ltdata",
        "select cast(sunmars_km as real), cast(earthmars_km as real), cast(sunmarsearthangle_deg as real), cast(solarconstantmars as real), cast(eclipseduration_min as real), cast(occultationduration_min as real) from ltdata where ut_ms = 1430006400000"
    ).await;
}