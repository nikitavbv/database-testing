use crate::dataset::{Dataset, DatasetRow, DatasetColumn};

#[derive(Debug)]
pub struct DatasetDiff {
    pub columns: Vec<DatasetColumn>,
    pub missing_columns: Vec<String>,
    pub extra_columns: Vec<String>,

    pub matched_rows: usize,

    pub rows_diff: Vec<RowDiff>,
    pub missing_rows: Vec<DatasetRow>,
    pub extra_rows: Vec<DatasetRow>,
}

#[derive(Debug)]
pub struct RowDiff {
    pub expected: DatasetRow,
    pub actual: DatasetRow,

    pub column_diff: Vec<ColumnDiff>,
}

#[derive(Debug)]
pub struct ColumnDiff {

    pub name: String,
    pub expected: String,
    pub actual: String,
}

//noinspection DuplicatedCode
pub fn compare_datasets(expected: Dataset, actual: Dataset) -> DatasetDiff {
    let expected_column_names: Vec<String> = expected.columns.iter().map(|v| v.name.clone()).collect();
    let actual_column_names: Vec<String> = actual.columns.iter().map(|v| v.name.clone()).collect();

    let mut missing_columns = expected_column_names.clone();
    for column in &actual_column_names {
        if let Some(index) = missing_columns.iter().position(|v| v == column) {
            missing_columns.remove(index);
        }
    }

    let mut extra_columns = actual_column_names.clone();
    for column in &expected_column_names {
        if let Some(index) = extra_columns.iter().position(|v| v == column) {
            extra_columns.remove(index);
        }
    }

    let rows_diff: Vec<RowDiff> = expected.rows.iter().zip(actual.rows.iter())
        .map(|(expected_row, actual_row)| diff_rows(expected_row, actual_row, &actual.columns))
        .filter(|v| v.is_some())
        .map(|v| v.unwrap())
        .collect();

    let matched_rows = actual.rows.len().min(expected.rows.len()) - rows_diff.len();

    let missing_rows = if actual.rows.len() < expected.rows.len() {
        expected.rows[actual.rows.len()-1..].to_vec()
    } else {
        Vec::new()
    };

    let extra_rows = if actual.rows.len() > expected.rows.len() {
        actual.rows[expected.rows.len()-1..].to_vec()
    } else {
        Vec::new()
    };

    DatasetDiff {
        columns: actual.columns,
        missing_columns,
        extra_columns,

        matched_rows,
        rows_diff,
        missing_rows,
        extra_rows
    }
}

fn diff_rows(expected: &DatasetRow, actual: &DatasetRow, columns: &Vec<DatasetColumn>) -> Option<RowDiff> {
    let column_diff: Vec<ColumnDiff> = columns.iter()
        .map(|column| column.name.clone())
        .map(|name| (expected.values.get(&name), actual.values.get(&name), name.clone()))
        .filter(|v| v.0.is_some() && v.1.is_some())
        .filter(|v| v.0.unwrap() != v.1.unwrap())
        .map(|v| ColumnDiff {
            name: v.2.clone(),
            expected: v.0.as_ref().unwrap().to_string(),
            actual: v.1.as_ref().unwrap().to_string(),
        })
        .collect();

    if column_diff.len() == 0 {
        return None;
    }

    Some(RowDiff {
        expected: expected.clone(),
        actual: actual.clone(),
        column_diff
    })
}