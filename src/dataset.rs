use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, BufRead};

#[derive(Debug, Clone)]
pub struct Dataset {

    pub columns: Vec<DatasetColumn>,
    pub rows: Vec<DatasetRow>,
}

#[derive(Debug, Clone)]
pub struct DatasetColumn {

    pub name: String,
}

#[derive(Debug, Clone)]
pub struct DatasetRow {

    // column -> value
    pub values: HashMap<String, String>
}

impl DatasetRow {

    pub fn as_csv(&self, columns: &Vec<DatasetColumn>) -> String {
        columns.iter()
            .map(|column| column.name.clone())
            .map(|name| self.values.get(&name).unwrap_or(&"???".to_string()).clone())
            .collect::<Vec<String>>()
            .join(";")
    }
}

pub fn load_dataset(name: &str) -> Dataset {
    let file = File::open(format!("expected/{}.csv", name)).expect("failed to open file with expected data");
    let reader = BufReader::new(file);

    let mut columns: Option<Vec<DatasetColumn>> = None;
    let mut rows = Vec::new();

    for line in reader.lines() {
        if let Ok(line) = line {
            if line.is_empty() {
                continue;
            }

            if let Some(columns) = &columns {
                let values: Vec<String> = line.split(";").map(|v| v.to_string()).collect();
                let mut row: HashMap<String, String> = HashMap::new();

                for index in 0..columns.len() {
                    row.insert(columns.get(index).unwrap().name.clone(), values.get(index).unwrap().clone());
                }

                rows.push(DatasetRow {
                    values: row
                });
            } else {
                columns = Some(line.split(";").map(|name| DatasetColumn {
                    name: name.to_string(),
                }).collect());
            }
        }
    }

    Dataset {
        columns: columns.unwrap(),
        rows
    }
}