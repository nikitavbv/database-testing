use tokio_postgres::{NoTls, Client, Row};
use crate::dataset::{Dataset, DatasetColumn, DatasetRow};
use tokio_postgres::types::Type;
use std::collections::HashMap;

pub async fn connect_to_database() -> Client {
    let (client, connection) = tokio_postgres::connect("host=localhost user=postgres password=postgres_password dbname=postgres", NoTls).await
        .expect("failed to connect to database");

    tokio::spawn(connection);

    client
}

pub fn rows_to_dataset(rows: Vec<Row>) -> Dataset {
    if rows.len() == 0 {
        panic!("Cannot convert empty response to dataset");
    }

    let columns = rows.get(0).unwrap().columns();
    let mut dataset_columns = Vec::new();
    let mut column_types: Vec<Type> = Vec::new();
    let mut dataset_rows = Vec::new();

    for column in columns {
        dataset_columns.push(DatasetColumn {
            name: column.name().to_string(),
        });
        column_types.push(column.type_().clone());
    }

    for row in &rows {
        let mut dataset_row_values: HashMap<String, String> = HashMap::new();

        for i in 0..columns.len() {
            let value: String = match column_types.get(i).unwrap() {
                &Type::TEXT => {
                    let value: &str = row.get(i);
                    value.to_string()
                },
                &Type::INT4 => {
                    let value: i32 = row.get(i);
                    format!("{}", value)
                },
                &Type::INT8 => {
                    let value: i64 = row.get(i);
                    format!("{}", value)
                },
                &Type::FLOAT4 => {
                    let value: f32 = row.get(i);
                    format!("{}", value)
                },
                other => {
                    panic!("Unknown column type: {}", other);
                }
            };

            dataset_row_values.insert(dataset_columns.get(i).unwrap().name.clone(), value);
        }

        dataset_rows.push(DatasetRow {
            values: dataset_row_values,
        });
    }

    Dataset {
        columns: dataset_columns,
        rows: dataset_rows,
    }
}